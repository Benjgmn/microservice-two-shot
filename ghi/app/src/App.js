import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './shoe_list';
import ShoeDetail from './shoe_detail';
import CreateShoe from './create_shoe';
import ShoesPage from './shoesBins';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList />} />
          <Route path="shoes/shoesBins" element={<ShoesPage />} />

          <Route>
          <Route path="/shoes/create" element={<CreateShoe />} />
          <Route path="/shoes/:id" element={<ShoeDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
