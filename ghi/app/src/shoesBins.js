import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const ShoesPage = () => {
  const [shoes, setShoes] = useState([]);
  const [bins, setBins] = useState({});

  const deleteShoe = async (id) => {
    const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, {
      method: 'DELETE',
    });

    if (response.ok) {
      fetchShoes();
    } else {
      console.error('Failed to delete shoe', response);
    }
  }

  const fetchShoes = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);

      // Organize shoes by bins
      let bins = {};
      data.shoes.forEach(shoe => {
        if (!bins[shoe.bin.id]) {
          console.log(`Creating bin_number for ${shoe.bin.id}`);
          bins[shoe.bin.id] = [shoe];
        } else {
          bins[shoe.bin.id].push(shoe);
        }
      });

      setBins(bins);
    } else {
      console.error('Failed to fetch shoes', response);
    }
  }

  useEffect(() => {
    fetchShoes();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center">
        <h1 className="display-5 fw-bold">Shoe List</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Here is a list of bins and the shoes that are in each bin
          </p>
        </div>
      </div>

      <div className="container">
        <h2>Shoes</h2>
        {Object.entries(bins).map(([id, shoes]) => (
          <div key={id}>
            <h3>Bin: {id}</h3>
            <div className="row">
              {shoes.map((shoe) => (
                <div key={shoe.id} className="col">
                  <div className="card">
                    <img src={shoe.picture_url} className="card-img-top" alt="" />
                    <div className="card-body">
                      <h5 className="card-title">{shoe.model_name}</h5>
                      <p className="card-text">{shoe.manufacturer}</p>
                      <Link to={`/shoes/${shoe.id}`} className="btn btn-primary">
                        Details
                      </Link>
                      <button onClick={() => deleteShoe(shoe.id)} className="btn btn-danger">Delete shoe</button>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default ShoesPage;
