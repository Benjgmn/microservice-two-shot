import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';

function ShoeDetail() {
  const [shoe, setShoe] = useState(null);
  const { id } = useParams();
  
  useEffect(() => {
    async function loadShoe() {
      const response = await fetch(`http://localhost:8080/api/shoes/${id}/`);
      if (response.ok) {
        const data = await response.json();
        setShoe(data);
      } else {
        console.error(response);
      }
    }
    loadShoe();
  }, [id]); // This array with `id` means the effect will run whenever `id` changes

  if (shoe === null) {
    return <div>Loading...</div>; // Or some other placeholder
  } else {
    return (
        <>
        <div className="container">
          <div className="card">
            <img src={shoe.picture_url} className="card-img-top" alt="" />
            <div className="card-body">
              <h5 className="card-title">{shoe.model_name}</h5>
              <p className="card-text">{shoe.manufacturer}</p>
              <p className="card-text">{shoe.color}</p>
              <Link to={`/shoes`} className="btn btn-primary">
                Back to shoes list
              </Link>
            </div>
          </div>
        </div>
        </>
    );
  }
}

export default ShoeDetail;
