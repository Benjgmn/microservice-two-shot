import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

// Change this to the path of your image based on your project structure
// import shoesImage from './shoes_image.jpg';

const ShoesPage = () => {
  const [shoes, setShoes] = useState([]);

  useEffect(() => {
    async function fetchShoes() {
      const response = await fetch('http://localhost:8080/api/shoes/');
      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
      } else {
        console.error('Failed to fetch shoes', response);
      }
    }

    fetchShoes();
  }, []);

  // Function to chunk the array
  const chunk = (arr, size) =>
    Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
      arr.slice(i * size, i * size + size)
    );

  // Create chunks of 3 shoes
  const shoesChunks = chunk(shoes, 3);

  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center">
        <img className="d-block mx-auto mb-4"  alt="" width="600" />
        <h1 className="display-5 fw-bold">Shoe Store!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Welcome to our shoe store. Explore the best shoes from top brands.
          </p>
        </div>
      </div>

      <div className="container">
        <h2>Shoes</h2>
        {shoesChunks.map((chunk, index) => (
          <div key={index} className="row">
            {chunk.map((shoe) => (
              <div key={shoe.id} className="col">
                <div className="card">
                  <img src={shoe.picture_url} className="card-img-top" alt="" />
                  <div className="card-body">
                    <h5 className="card-title">{shoe.model_name}</h5>
                    <p className="card-text">{shoe.manufacturer}</p>
                    <Link to={`/shoes/${shoe.id}`} className="btn btn-primary">
                        Details
                    </Link>
                  </div>
                </div>
              </div>
            ))}
          </div>
        ))}
      </div>
    </>
  );
};

export default ShoesPage;
