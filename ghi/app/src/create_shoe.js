import React, { useState } from 'react';

function CreateShoe() {
  const [manufacturer, setManufacturer] = useState('');
  const [modelName, setModelName] = useState('');
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [binId, setBinId] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    const newShoe = {
      manufacturer,
      model_name: modelName,
      color,
      picture_url: pictureUrl,
      bin_id: parseInt(binId), // ensure binId is sent as an integer
    };
    
    const response = await fetch('http://localhost:8080/api/shoes/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newShoe),
    });
    
    if (response.ok) {
      const data = await response.json();
      console.log(data);
    } else {
      console.error(response);
    }
  };

  return (
    <div className="container">
      <h1 className="my-4">Create New Shoe</h1>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label className="form-label">Manufacturer:</label>
          <input type="text" className="form-control" value={manufacturer} onChange={e => setManufacturer(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Model Name:</label>
          <input type="text" className="form-control" value={modelName} onChange={e => setModelName(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Color:</label>
          <input type="text" className="form-control" value={color} onChange={e => setColor(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Picture URL:</label>
          <input type="text" className="form-control" value={pictureUrl} onChange={e => setPictureUrl(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Bin Number:</label>
          <input type="number" className="form-control" value={binId} onChange={e => setBinId(e.target.value)} />
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
    </div>
  );
}

export default CreateShoe;
