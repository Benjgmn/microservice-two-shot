from django.db import models



# Create your models here.
from django.urls import reverse


class BinVO(models.Model): 
    closet_name = models.CharField(max_length=100, null=True)
    import_href = models.CharField(max_length=500)
   


class Shoes(models.Model):
    href = models.CharField(max_length=500, null=True)
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=500)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE
        )

    def __str__(self):
        return self.model_name
    
   