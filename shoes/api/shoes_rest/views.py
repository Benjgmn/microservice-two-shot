from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse, HttpResponse
from .models import Shoes, BinVO
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist



import json


from common.json import ModelEncoder


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "id",
        # "bin"
        ]
                # Changing the "id" to "bin_id" broke everything and caused a cors I it broke everyhting because the BinVO object does not have a "bin_id field"
class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [ 
        "manufacturer",
        "model_name",
        "color", 
        "picture_url",
        "id",
        "bin"]
    encoders = {
        "bin": BinVOEncoder(),
    }



@csrf_exempt
@require_http_methods(["GET", "POST", "DELETE"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        print(shoes)
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesDetailEncoder,
        )
    # ...
#     else:
#         content = json.loads(request.body)
#         print(content)
#         try:
#             bin_object = BinVO.objects.get(id=content["bin_id"])
#             print(bin_object)
#             content["bin"] = bin_object  # assigning the id of the bin_object to the bin field in the shoe instance
            
#         except BinVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid bin id"},
#                 status=400,
#             )
# ...

    else:
        content = json.loads(request.body)
        # setting the varibale content to a dictionary that is the json body
        print(content)
        try:
            # bin_object = BinVO.objects.get(import_href=content["import_href"])
            bin_object = BinVO.objects.get(bin_id=content["id"])
            print(bin_object)
            content["bin"] = bin_object
            print(bin_object)
            del content['import_href']
            # need to delete the "import_href" key because it is not in the bin model
            print(bin_object)
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )


  
    

@csrf_exempt
@require_http_methods(["GET", "PUT" , "DELETE"])
def api_show_shoes(request, pk):
    try:
        shoe = Shoes.objects.get(id=pk)
    except ObjectDoesNotExist:
        return JsonResponse({"message": "Shoe not found"}, status=404)

    if request.method == "GET":
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        for attr, value in content.items():
            if attr in ['manufacturer', 'model_name', 'color', 'picture_url']:
                setattr(shoe, attr, value)
        shoe.save()
        return JsonResponse(
            shoe,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        shoe.delete()
        return HttpResponse(status=204)
